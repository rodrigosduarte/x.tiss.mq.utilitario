﻿using RabbitMQ.Client;
using System.Text;

namespace X.Tiss.MQ.Utilitario
{
    public class MQConnect
    {
        private string _Host;
        private int _Porta;
        private string _Usuario;
        private string _Senha;

        public MQConnect(string Host, int Porta, string Usuario, string Senha)
        {
            _Host = Host;
            _Porta = Porta;
            _Usuario = Usuario;
            _Senha = Senha;
        }

        public ConnectionFactory Get()
        {
            var factory = new ConnectionFactory() { HostName = _Host, Port = _Porta, UserName = _Usuario, Password = _Senha };
            return factory;
        }

        public bool Mensagem(string fila, string key, string msg)
        {
            var factory = new ConnectionFactory() { HostName = _Host, Port = _Porta, UserName = _Usuario, Password = _Senha };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: key,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = Encoding.UTF8.GetBytes(msg);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "",
                                     routingKey: key,
                                     basicProperties: properties,
                                     body: body);
            }

            return true;
        }
     }
}
